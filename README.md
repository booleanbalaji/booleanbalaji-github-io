[![Netlify Status](https://api.netlify.com/api/v1/badges/6436a466-bfb8-484a-9345-01ca334355f4/deploy-status)](https://app.netlify.com/sites/booleanbalaji/deploys)
# gatsby-starter-blog
Gatsby starter for creating a blog

Install this starter (assuming Gatsby is installed) by running from your CLI:

`gatsby new gatsby-blog https://github.com/gatsbyjs/gatsby-starter-blog`

Or [view the live demo here](https://gatsby-starter-blog-demo.netlify.com/).

## Running in development
`gatsby develop`
