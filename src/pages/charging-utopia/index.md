---
title: Welcome to the charging Uptopia.
date: "2017-10-01T22:12:03.284Z"
---

Recently, I upgraded(?) to a Samsung Galaxy S8 by trading in an iPhone 7 256GB and an old OnePlus 3T. The rationale at that time was the beautiful screen in the S8, wireless charging, 64GB of storage with microSD card expansion and what not. Basically the S8 is the swiss army knife of modern mobile phones!

Why? Well, look at the feature set of the S8:

Want waterproofing? Check.

A great screen? Check.

Need ANT+ and Bluetooth 5.0 support? Check.

Decent battery life, Quick Charging 2.0 and wireless charging? Check.

The S8 tries to be the jack of all trades and it has succeeded in being so. Somewhat. The feathers on Samsung’s cap start falling when you use the device for a while.

The fast charging, while using the QC 2.0 standard by Qualcomm, strangely works only when the device is completely idle. Yep, it will not fast charge as well as you hoped even when simple things like app updates or music playback is in progress. This makes it completely useless in cases where you have quickly recharge, but also cannot keep your phone aside. While Anker and Aukey have a few portable power banks which have some degree of QC implemented, charging when the screen is on, makes them completely useless.

Moving on to wireless charging, it is great that Apple has joined the bandwagon, but Samsung still uses a relatively old spec for Qi Wireless charging. The bundled wireless charger is a great aesthetic, but is only useful when on your bedside or work desk and you have hours to spare, watching your phone slowly charge up. This is an even bigger problem with larger phones like the Galaxy Note 8 and even older ones like the Note 5.

Further, Samsung’s ‘fast’ wireless charging only works when bundled with an adapter which supports QC 2.0 and well, the bundled wireless charger doesn't include the power adapter. The compatibility is further complicated when you realise almost all the wireless chargers use the legacy microUSB port and only the Samsung fast wireless charger uses the USB-C interface.

Sigh, it is at these times, I wish all manufactures used USB-C PD spec on their devices. If you’re not familiar with the spec, it was widely made popular by the now defunct Nexus line of phones and the trend continues with the spec found on the Pixel phones and even Apple’s devices. If you’re curious, the ‘fast’ charging Apple talks about in the new iPhone 8 and above, is delivered with USB-C PD, which is strange in a way.

Restrictions on USB-C PD force manufacturers to use the USB-C interface on both ends of the charging cable. Apple now sells a lightning to USB-C cable and a USB-C PD charging adapter to go with it. This not only means that your iPhone would be able to charge faster than the dumb 5W adapter bundled in the box, but that the lighting port is now rendered to just a different configuration of USB-C.

The underlying chip in the lightning port, is USB-C.

Welcome to the charging Utopia.
