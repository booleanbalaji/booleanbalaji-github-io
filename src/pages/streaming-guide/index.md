---
title: The ultimate guide to streaming any content to any device, anywhere!
date: "2016-02-11T22:12:03.284Z"
---

📺_Chromecast, Plex, HTPC, Roku and more. We have a solution for everyone!_

_Originally published on [Digit.in](https://www.digit.in/internet/the-ultimate-guide-to-streaming-any-content-to-any-device-anywhere-29002.html)_

Organising your existing collection of media or updating can be a huge pain if you don’t have the right tools to do it. Your media collection can span gigabytes or even terabytes stored on a hard disk, but how do you make sure that content is available with you wherever you are? This guide will tell you just that. Organization of content has become very important now that there are tons of TV shows and movies you can watch, and most of it can be found on the internet, so you’ll need to get everything connected. There are several options for streaming content and managing your downloaded content, most of which are easy and will work out of the box, but there are some which will need some technical knowledge. For instance, a simple situation would be if you have a good Wi-Fi router capable of delivering a large bandwidth and have videos and pictures stored on a computer connected to the same network. By using a streaming stick such as the Chromecast or the Roku, and setting up a media server on the computer, you can easily pass content from the computer to the TV wirelessly.

## Plex - Your one stop shop

Plex is a two part software, one is a server and the other is a client app. Plex organizes your media collection-movies, music and TV shows, and shares the collection over your local network or over the internet. This will enable any of your laptops, tablets, mobile phones and streaming devices to access the wide range of content in your hard disk. That’s not all, Plex also pulls metadata for the content. For example, if you have a movie, Plex searches through various databases to find information about the movie such as the poster, the cast, crew and even the synopsis and ratings. Plex claims to be “One window into all your personal media. No matter where you are” and it is mostly true.

Plex is a two part software, one is a server and the other is a client app. Plex organizes your media collection-movies, music and TV shows, and shares the collection over your local network or over the internet. This will enable any of your laptops, tablets, mobile phones and streaming devices to access the wide range of content in your hard disk. That’s not all, Plex also pulls metadata for the content. For example, if you have a movie, Plex searches through various databases to find information about the movie such as the poster, the cast, crew and even the synopsis and ratings. Plex claims to be “One window into all your personal media. No matter where you are” and it is mostly true.

The advantage of using a media server such as plex, is that it can transcode content. So if you are looking to watch your content on a device which doesn’t particularly support the format of the files, plex will convert it to a format which you can play. Although this requires a higher powered server, it is useful if you have your content in a large number of formats. Plex has client apps for the Amazon Fire TV, Android TVs, the now obsolete Google TV, Roku, Samsung Smart TVs, iOS, Windows Phone 8 and Windows 8.1. Although some of them are free to use, you’ll need to shell out a small amount to be able to stream your content on mobile devices.

## How to set up the mothership - All your data resides here

A plex server is a dedicated machine which stores all your media on its hard disk, runs the plex server software to organize the media and allows devices on the network to pull the media from it. Your server could be your own laptop or an old PC lying around or a dedicated NAS or a raspberry Pi setup. For the purpose of this guide, we will assume that you have a dedicated PC with windows running and all your media on the internal or an external drive. This system needs to be connected to your router.

Now, to install the plex server, head over to Plex.tv and download the server side software. While doing this, make sure that your media is named and organized correctly so that plex can recognize it. This is important as, plex uses the existing metadata to find more information about your media. If your metadata is incorrect or missing, it will lead to incorrect matching, causing you severe emotional trauma and distress when trying to stream content.

## Adding files

The setup of the server is pretty easy, with a well guided installation. You can add your libraries for Movies, TV shows and Music and point these libraries at your existing collection. Plex will scan your collection for media it can identify and organize it. Once you setup a plex server, you are good to go for basic streaming. You can connect your Chromecast and directly cast from your server if needed. For a more complete solution and automatic downloading, read on.

## Streaming them across

Sharing files over the network was one of the most fundamental purposes of a computer network and the focus has now shifted to streaming media and playback on different devices rather than sharing the files and expecting each device to support the playback of multiple file formats. Today, devices such as the Roku, Google Chromecast and various other streaming devices allow you to make your dumb TV, smart. These devices usually connect through the HDMI port on your TV and connect to the local network through Wi-Fi or Ethernet. You can access all your shared media then, in a beautiful interface on the TV and enjoy streaming your media.

## Get more content

There are several ways to get your content. You can subscribe to the many streaming services available or you can download them off the internet. Although some of the content on the internet can be pirated, you will find several Movies and TV shows which are available freely. Torrents and usenets can be used to get these content shared by people around the world. usenets and Torrents offer the same kind of content, but are vastly different when it comes to the method of getting that content.

## Usenets

Usenets are a type of threaded discussion boards, where users can post and reply to messages. The one area where usenets are different from regular forums, is that it’s highly distributed. There is no centralized server, and the data is passed among the many servers continuously. owing to the large amount of data that is being shuffled around, the servers have a retention period. This can range from an hour to several months or years. After the retention period passes, the files and messages are deleted off the servers. Usenets are a solid alternatives to torrents, especially because it’s much more secure than a peer to peer network.

## Torrents

Torrents provide a wide range of free, copyright free content, but is ridden with illegal rips of movies and TV shows. So, you need to be careful when downloading the free content and make sure you are not breaking any laws. Peer to peer networks allow users to create a network among many clients. There is no fixed server, and each node acts as a server and a client when needed. Peer to peer networks are decentralized and are hardly regulated. Its advisable to use a VPN when downloading torrents as there is a lot of malicious and illegal content, which might land you in a legal kerfuffle. Using VPN services will severely affect the bandwidth which you get when downloading, but it is the price you pay for anonymity and security.

## Setting up a dedicated Home Theatre

A HTPC or a Home Theatre Portable Computer is dedicated computer for media consumption. It is usually connected directly to a large monitor or a television and has media on its internal drives or can be driven by a NAS. HTPCs are fairly common among enthusiasts who repurpose old parts to create a new PC. If you consume a lot of media and don’t like connecting a lot of devices to your TV.  

HTPCs are generally low powered and single purpose. It will run a media server, and can be directly connected to the TV or you can use a streaming stick to stream the content from the HTPC to you TV, saving some clutter. Some HTPCs can be really high powered, with a dedicated graphics card, if you need to run 4K or 3D content, and are particular about high quality content and have a TV to match it.

## Alternatives

SickBeard is an application written in Python, which does the same thing as Sonarr, albeit it has a less slick UI than Sonarr. SickRage is a fork of Sickbeard. It brings a lot more functionality and a better interface to your streaming nirvana endeavour. There are a number of alternatives for Plex, such as Kodi, OSMC, OpenELEC and Emby, which are media servers, and to some extent, organize your media quite well.

## CouchPotato

CouchPotato is a content manager for movies. You can mark movies ‘wanted’ and it will scan its various indexers for available files and begin downloading the files for you. After the downloads, you can set it to rename and move the files to the appropriate folders. It also suggest movies from popular sources like IMDb and bluray.com, so if you’re not sure what you’re looking for, the suggestions will help you decide. CouchPotato also gives you a nice extension for Safari, Chrome and Firefox, so if you see a movie you fancy, you can use the extension to mark it wanted. CouchPotato is available as an installer for Windows and OSX, but if you are running Linux or have a habit of taking things apart, you need to compile from source.

To get started with CouchPotato, head over to CouchPota.to to download the installer. Next, you need to open the web interface at localhost:5050/wizard, create a username and password and leave the port as is. You can set the directory to save the torrent files. Adding some services from which you want to pull torrent services in the next step will make it easier for CouchPotato to find your movies online. Now, after you select the Movies you want, CouchPotato will check the services and download the torrent files to the folder.

Your torrent application can now be set to check this folder for new .torrent files and add them to download immediately. In BitTorrent or uTorrent, you can do this by going to Options>Preferences>Directories and check the box for Automatically load .torrents from. You can also set uTorrent to move the downloaded files to another folder. This is useful in the case of a raspberry Pi or a HTPC with limited internal storage. After you set this up, whenever a movie which you wanted in CouchPotato becomes available, it is automatically downloaded and moved to the folder you want.

## Sonarr

Sonarr is a light application that works on Windows, Mac and Linux, and manages your collection of TV shows. Once Sonarr is pointed to the directories where your collection resides, it scans the folders and tells you which episodes are missing from your vast collection. You can also set to monitor some of your TV shows, and Sonarr will keep checking various indexers, and download the necessary files when it becomes available. Sonarr is very flexible and allows you to select which quality you want for each TV show, customize the indexers, download locations, and subtitles. You can download Sonarr and use their installer for easy installation and set up.

## OpenELEC for Raspberry Pi

OpenELEC is a just enough operating system for computers which are low powered and embedded systems. OpenELEC runs on the raspberry PI and uses Kodi as the media server. You will see the Kodi interface at boot up and will not have to configure any settings in the operating system. You will just need to add the location of your folders and set the metadata scrapers and you’re good to go.

## OSMC for the Raspberry Pi or Apple TV

OSMC or Open Source Media Center is an aftermarket firmware for the Raspberry Pi and the Apple TV, which provides much more features than the stock firmware. OSMC adds a beautiful interface on top to Kodi (formerly XBMC). OSMC can be easily installed in all platforms by using the installer or directly using the images on the SD card in case of the Raspberry Pi. OSMC is one of the best Operating Systems to run on the Raspberry Pi, and it is much better than the original Kodi interface, which you get with OpenELEC or manually installing Kodi on top of an existing operating system.

You can find detailed steps for all the above apps [here](https://github.com/booleanbalaji/StreamingHowTo).
