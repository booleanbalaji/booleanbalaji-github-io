import React from 'react'

// Import typefaces
import 'typeface-montserrat'
import 'typeface-merriweather'

import profilePic from './profile-pic.jpg'
import { rhythm } from '../utils/typography'

class Bio extends React.Component {
  render() {
    return (
      <div
        style={{
          display: 'flex',
          marginBottom: rhythm(2.5),
        }}
      >
        <img
          src={profilePic}
          alt={`Abhishek Balaji`}
          style={{
            marginRight: rhythm(1 / 2),
            marginBottom: 0,
            width: rhythm(2),
            height: rhythm(2),
          }}
        />
        <p>
          Written by <strong><a href="https://www.linkedin.com/in/abhishekbalaji">Abhishek Balaji</a></strong> who lives and works in Bangalore @ <a href="https://hasgeek.com">HasGeek</a>.{' '}
          <a href="https://twitter.com/booleanbalaji">
            You should follow him on Twitter
          </a>
        </p>
      </div>
    )
  }
}

export default Bio
